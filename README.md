# Investigating inter-individual gene expression variability depending on age, diet and gender

What happens to gene expression variability depending on age, diet and gender.

## Description

The changes in mean gene expression and its variability have been and are still being widely studied at present. Though gene expression variability is a popular research subject, the coefficient of variation (CV = standard deviation divided by mean) is usually assumed to be the best measure, and therefore it is the most used way for calculating variability in the field. Here, we investigate multiple measures for calculating gene expression variability and try to determine whether the coefficient of variation (CV) is really the most robust measure to calculate it. We assess the power of each used measure for variability by examining how the variability of certain groups of genes relate to their biological functions, and whether variability estimates exhibit results which are to be expected. The most importantly, results show that in some cases, the median absolute deviation divided by the mean (MADM) seems to perform better, while in other cases, the CV performs better. The quantile coefficient of dispersion (QCOD) in our investigation showed relatively poor results and therefore this measure is closed out from being a candidate for best measure. The general take away message here, is that most of the evidence of biological significance of variability estimates seems to be in favor of the CV measure.
### Prerequisites

What things you need to install the software and how to install them. Some programs and R libraries must be installed to get this script up and running.

Programming languages
```
- R
- Python
```

Libraries for R
```
- gProfiler
- glue
- bioMart
- ggplot2
- org.Mm.eg.db
- org.Hs.eg.db
```

## Built With

* [R](https://www.r-project.org) - The R Project for statistical computing
* [Python](https://www.python.org/) - The python programming language

## Authors

* **Rene Bults**


## Acknowledgments

* Victor Guryev for providing the internship project and guidance
* Tristan de Jong for guiding me through the project
* Jasper Bosman for being my hanze supervisor


