#!/usr/bin/env Python3

"""
This script extracts whatever tissue the user wants
from the complete human GTEx data. The script takes
the count table file as first command line argument, and
the phenotype data as second command line argument.

Author: Rene Bults
Date: 06-12-18
Version: 1.0
"""

import sys
from operator import itemgetter


def read_count_data():
    return sys.argv[1]


def read_phenotype_data():
    return sys.argv[2]


def process_phenotype_data(phenotypes):
    liver_samples = []
    with open(phenotypes) as pheno:
        while True:
            line = pheno.readline()
            line = line.strip().split("\t")
            if len(line) > 5:
                if "Liver" in line[5]:
                    liver_samples.append(line[0])
            else:
                break

            if not line:
                break

    return liver_samples


def process_data(count_data, liver_samples):
    with open(count_data) as read:

        # skip first two lines to reach header
        read.readline()
        read.readline()
        header = read.readline().strip().split("\t")    # get header

        liver_indexes = [header.index(i) for i in liver_samples if i in header]     # retrieve index values
        liver_indexes.insert(0, 0)  # insert 0 into indexes to also take gene names

        liver_sample_file = open("liver_samples.csv", "a")      # open output file

        read.seek(0)    # go back to top of file
        while True:
            read.readline()
            read.readline()
            line = read.readline().strip().split("\t")

            if len(line) > 2:
                liver_counts = [*itemgetter(*liver_indexes)(line), ]    # retrieve values of indexes for every line
                liver_counts[0] = liver_counts[0][0:15]                 # strip the version number away

                liver_sample_file.write(",".join(liver_counts))     # write information to file
                liver_sample_file.write("\n")
            else:
                print("Skipping empty line...")
                break

            if not line:
                break


def remove_versions(liver_samples):
    with open(liver_samples, "r") as liver:
        for line in liver:
            line = line.split(",")
            line[0:16] = line[0:14]


def main():
    count_data = read_count_data()
    phenotypes = read_phenotype_data()
    liver_sams = process_phenotype_data(phenotypes)
    processed_data = process_data(count_data, liver_sams)
    #remove_versions("liver_samples.csv")


if __name__ == "__main__":
    sys.exit(main())
