###############################################################
# title:                Functions                             #
# Author:               Rene Bults                            #
# Institute:            ERIBA                                 #
# School:               Hanze university of applied sciences  #
# Study:                Bachelor in bio-informatics           #
# Class:                BFV-4                                 #
# Version:              1.0                                   #
# Supervisors (ERIBA):  Victor Guryev, Tristan de Jong        #
# Supervisors (Hanze):  Jasper Bosman, Martijn Herber         #  
###############################################################

# set the proper working directory
#######################################################################
setwd("expression-noise-analysis/Scripts and results/")
#######################################################################

# install packages if they are not installed yet
#######################################################################
list.of.packages <- c("edgeR", "limma", "glue", "gProfileR", "CePa", "BiocManager", "gamlss", "tibble", "ggrepel", "MASS")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages, repos = "http://cran.us.r-project.org")
#######################################################################


# loading required packages
#######################################################################
library(edgeR)
library(limma)
library(glue)
library(gProfileR)
library(CePa)
library(org.Hs.eg.db)
library(ggplot2)
library(org.Mm.eg.db)
library(biomaRt)
# library Rgraphviz
#######################################################################

# functions
#######################################################################
# function for calculating MAD
MAD.fun <- function(x){
  return(apply(x, 1, mad))
}

# function for calculating CV
CV.fun <- function(x){
  return(apply(x, 1, function(y){sd(y) / mean(y)}))
}

# function to calculate Quantile coefficient of dispersion
# quantile takes the exact integer, not a number chosen that exists in the dataset.
QCOD.fun <- function(x){
  return(apply(x, 1, function(y){(quantile(y, 0.75, 4) - quantile(y, 0.25, 4)) / (quantile(y, 0.75, 4) + quantile(y, 0.25, 4))}))
}

# function to calculate the MADM, the Median absolute deviation divided by the mean
MADM.fun <- function(x){
  return(apply(x, 1, function(y){mad(y) / mean(y)}))
}

# add a-value to colors
add.alpha <- function(col, alpha=1){
  apply(sapply(col, col2rgb)/255, 2, function(x) {rgb(x[1], x[2], x[3], alpha=alpha)})  
}

# functions for plotting the 4 combinations of diet and sex
# same gender, different diet
plot.sex.DO <- function(x, method){
  nums.sex <- seq(1, length(DO.list), 2)
  DO.labels <- list("DO.HF.M", "DO.SC.M", "DO.HF.F", "DO.SC.F")
  for(i in nums.sex){
    colors <- ifelse(unlist(x[i]) / unlist(x[i + 1]) > 2, "red", "black")
    colors <- ifelse(unlist(x[i]) / unlist(x[i + 1]) < 0.6, "forestgreen", colors)
    ratio <- unlist(x[i]) / unlist(x[i + 1])
    col.alpha <- add.alpha(colors, alpha = 0.2)
    plot(unlist(x[i]), unlist(x[i+1]), xlim = c(0, max(unlist(x[i]))), ylim = c(0, max(unlist(x[i]))), xlab = DO.labels[i], ylab = DO.labels[i+1], col = col.alpha, main = glue("Diet specific, {method}"), lty = 2, pch = 20)
    abline(a = 0, b = 1)
    num.red <- table(colors)["red"]
    num.green <- table(colors)["forestgreen"]
    num.black <- table(colors)["black"]
    cols <- data.frame(colors)
    red.genes <- colors[cols$colors == "red"]
    green.genes <- colors[cols$colors == "forestgreen"]
    #write(x = names(red.genes), file = glue("top_bot/red_{method}_{DO.labels[i]}_{DO.labels[i+1]}.csv"))
    #write.csv(x = names(green.genes), file = glue("top_bot/green_{method}_{DO.labels[i]}_{DO.labels[i+1]}.csv"))
    write.csv(x = ratio, file = glue("mouse_variability_ratios/ratios_diversityOutbreds/ratio_DO_{method}_{DO.labels[i]}_{DO.labels[i+1]}_RATIO.csv"))
    corr <- cor.test(unlist(x[i]), unlist(x[i+1]))
    legend(x = "topleft", legend = c(glue("green: {num.green}"), glue("red: {num.red}"), glue("black: {num.black}"), glue("{corr[4]}")))
  }
}

# same diet, different gender
plot.diets.DO <- function(x, method){
  nums.diet <- seq(1, length(DO.list) - 2, 1)
  DO.labels <- list("DO.HF.M", "DO.SC.M", "DO.HF.F", "DO.SC.F")
  for(i in nums.diet){
    colors <- ifelse(unlist(x[i]) / unlist(x[i + 2]) > 2, "red", "black")
    colors <- ifelse(unlist(x[i]) / unlist(x[i + 2]) < 0.6, "forestgreen", colors)
    col.alpha <- add.alpha(colors, alpha = 0.2)
    ratio <- unlist(x[i]) / unlist(x[i + 2])
    plot(unlist(x[i]), unlist(x[i+2]), xlim = c(0, max(unlist(x[i]))), ylim = c(0, max(unlist(x[i]))), xlab = DO.labels[i], ylab = DO.labels[i+2], col = col.alpha, main = glue("Gender specific, {method}"), lty = 2, pch = 20)
    abline(a = 0, b = 1)
    num.red <- table(colors)["red"]
    num.green <- table(colors)["forestgreen"]
    num.black <- table(colors)["black"]
    cols <- data.frame(colors)
    red.genes <- colors[cols$colors == "red"]
    green.genes <- colors[cols$colors == "forestgreen"]
    corr <- cor.test(unlist(x[i]), unlist(x[i+2]))
    #write(x = names(red.genes), file = glue("top_bot/red_{method}_{DO.labels[i]}_{DO.labels[i+2]}.csv"))
    #write.csv(x = names(green.genes), file = glue("top_bot/green_{method}_{DO.labels[i]}_{DO.labels[i+2]}.csv"))
    write.csv(x = ratio, file = glue("mouse_variability_ratios/ratios_diversityOutbreds/ratio_DO_{method}_{DO.labels[i]}_{DO.labels[i+2]}_RATIO.csv"))
    legend(x = "topleft", legend = c(glue("green: {num.green}"), glue("red: {num.red}"), glue("black: {num.black}"), glue("{corr[4]}")))
  }
}

# function to plot. input is a list of data frames for which the cv is calculated
# or any other statistical method for variability
# this is for inbred, so only comparing diets (they are all male)
plot_NDO.fun <- function(x, method){
  pdf(file = glue("result_files/comparing the diets of inbreds using {method}.pdf"))
  par(mfrow = c(1, 1), pty = "s")
  looper <- seq(1, length(strains), 2)
  labels <- list("X.HF", "X.SC", "AJ.HF", "AJ.SC", "B.HF", "B.SC", "CA.HF", "CA.SC", "NOD.HF", "NOD.SC", "NZO.HF", "NZO.SC", "PWK.HF", "PWK.SC", "WSB.HF", "WSB.SC")
  titles <- list("X1", "-", "AJ", "-", "B6", "-", "CAST", "-", "NOD", "-", "NZO", "-", "PWK", "-", "WSB", "-")
  for(i in looper){
    colors <- ifelse(unlist(x[i]) / unlist(x[i + 1]) > 2, "red", "black")
    colors <- ifelse(unlist(x[i]) / unlist(x[i + 1]) < 0.5, "forestgreen", colors)
    col.alpha <- add.alpha(colors, alpha = 0.2)
    ratio <- unlist(x[i]) / unlist(x[i + 1])
    plot(unlist(x[i]), unlist(x[i + 1]), xlim = c(-0.1, 3), ylim = c(-0.1, 3), lty = 2, pch = 20, cex = 0.5, xlab = labels[i], ylab = labels[i + 1], main = titles[i], col = col.alpha)
    abline(a = 0, b = 1)
    num.red <- table(colors)["red"]
    num.green <- table(colors)["forestgreen"]
    num.black <- table(colors)["black"]
    corr <- cor.test(unlist(x[i]), unlist(x[i+1]))
    write.csv(x = ratio, file = glue("mouse_variability_ratios/ratios_inbreds/ratio_NDO_{method}_{labels[i]}_{labels[i+1]}_RATIO.csv"))
    cols <- data.frame(colors)
    red.genes <- colors[cols$colors == "red"]
    green.genes <- colors[cols$colors == "forestgreen"]
    legend(x = "topleft", legend = c(glue("green: {num.green}"), glue("red: {num.red}"), glue("black: {num.black}"), glue("{corr[4]}")))
  }
  dev.off()
}

# this part might be obsolete. im already calculating the ratios and doing gprofiler on the top and bottom 100 of them
div.fun <- function(x, meth){
  looper_do <- seq(1, length(DO.list), 2)
  for (i in looper_do){
    one <- unlist(x[i])
    two <- unlist(x[i + 1])
    result <- (one / two)
    write.csv(x = result, file = glue("result_files/result_{i}_{meth}.csv"))
  }
}

# function for plotting different ages against each other
plot_ages <- function(x, labels){
  for(i in 2:length(x)){
    colors <- ifelse(unlist(x[1]) / unlist(x[i]) > 2, "red", "black")
    colors <- ifelse(unlist(x[1]) / unlist(x[i]) < 0.5, "forestgreen", colors)
    col.alpha <- add.alpha(colors, alpha = 0.2)
    plot(x[[1]], x[[i]], xlab = labels[[1]], ylab = labels[[i]], 
         xlim = c(0, max(x[[i]])), ylim = c(0, max(x[[i]])), lty = 2, pch = 20, col = col.alpha, main = "Comparing the age of humans")
    abline(a = 0, b = 1)
  }
}


# function to get top and bot 100 ratios 
get_human_ratios_gprofiler <- function(x, m, sample, labels){
  for(i in 2:length(x)){
    rat <- x[[1]] / x[[i]]
    write.csv(x = x[[1]], file = glue("human_variabilities/var_{sample}_{m}_{labels[[1]]}.csv"))

    sorted <- sort(rat, decreasing = TRUE)
    
    write.csv(x = sorted, file = glue("human_variability_ratios/full_ratios_all_tissues/full_ratio_{sample}_{m}_{labels[[i]]}.csv"))
    
    top_100 <- head(sorted, 100)
    bot_100 <- tail(sorted, 100)
    
    write.csv(x = top_100, file = glue("human_variability_ratios/{sample}/{m}/top_100/{m}_{labels[[1]]}_{labels[[i]]}_top.csv"))
    gproftop <- gprofiler(names(top_100), organism = "hsapiens", src_filter = c("GO", "KEGG"))
    write.csv(x = gproftop, file = glue("human_variability_ratios/{sample}/{m}/top_100/gprofiler_results/{m}_{labels[[1]]}_{labels[[i]]}_top_gprof.csv"))
    
    write.csv(x = bot_100, file = glue("human_variability_ratios/{sample}/{m}/bot_100/{m}_{labels[[1]]}_{labels[[i]]}_bot.csv"))
    gprofbot <- gprofiler(names(bot_100), organism = "hsapiens", src_filter = c("GO", "KEGG"))
    write.csv(x = gprofbot, file = glue("human_variability_ratios/{sample}/{m}/bot_100/gprofiler_results/{m}_{labels[[1]]}_{labels[[i]]}_bot_gprof.csv"))
    
}}


# retrieve the ages of the human data
get_ages_fun <- function(age, counts){
  count = 0
  get_ages <- sapply(age[,1], function(x){       # sapply loops over every row, and returns true or false on every element of row
    if (sum(grepl(x, colnames(counts))) >= 1){  # by using grepl (searching for keyword in string). if the sum of the true/false count is higher than 1, the 
      count <<- count + 1                             # the corresponding age is returned. 
      return(paste(age[,3][count], sep = "_"))
      
    }else{
      count <<- count + 1
      return("NA")
    }
  })
}


# this will result in a list of ages, in the correct order,
# the rownames are the samples, the values are the ages
# the reason there are so many NA values, is that only the liver samples are taken here,
# and the ageing information in total is for all samples.


#######################################################################



